This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "d4science-iam-client"

## [v1.0.0-SNAPSHOT]
- Helps on basic retrieve of the AUTHN and AUTHZ form the IAM, hiding the use of the underling implementation. With dome utility helper functions for get the info from the tokens.
    - It already support for the use of the custom Keycloak's D4S mappers that maps/shrink the `aud` (and optionally also the resource access) to the value requested via dynamic scope (by default) and `X-D4Science-Context` HTTP header (can be only globally enabled via static flag) (#24701, #23356, #28084).
- Helps to obtain the token for the user and related tests (#28084)
- Helps to parse, verify and get D4Science related info from an OIDC access token (e.g. from bearer authorization header) (#28351)
