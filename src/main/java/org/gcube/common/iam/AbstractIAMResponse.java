package org.gcube.common.iam;

import java.util.HashSet;
import java.util.Set;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientException;
import org.gcube.common.keycloak.model.AccessToken;
import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.RefreshToken;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.io.jsonwebtoken.ExpiredJwtException;
import org.gcube.io.jsonwebtoken.security.SignatureException;

public class AbstractIAMResponse implements IAMResponse {

    private D4ScienceIAMClient iamClient;
    private TokenResponse tokenResponse;

    public AbstractIAMResponse(D4ScienceIAMClient iamClient, TokenResponse tokenResponse) {
        setIamClient(iamClient);
        setTokenResponse(tokenResponse);
    }

    public void setIamClient(D4ScienceIAMClient iamClient) {
        this.iamClient = iamClient;
    }

    public D4ScienceIAMClient getIamClient() {
        return iamClient;
    }

    public void setTokenResponse(TokenResponse tokenResponse) {
        this.tokenResponse = tokenResponse;
    }

    protected TokenResponse getTokenResponse() {
        return tokenResponse;
    }

    @Override
    public AccessToken getAccessToken() throws D4ScienceIAMClientException {
        try {
            return ModelUtils.getAccessTokenFrom(getTokenResponse());
        } catch (Exception e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    public String getAccessTokenString() {
        return getTokenResponse().getAccessToken();
    }

    @Override
    public boolean isExpired() throws D4ScienceIAMClientException {
        try {
            return ModelUtils.getAccessTokenFrom(getTokenResponse()).isExpired();
        } catch (Exception e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    public String getRefreshTokenString() {
        return getTokenResponse().getRefreshToken();
    }

    @Override
    public boolean canBeRefreshed() throws D4ScienceIAMClientException {
        try {
            RefreshToken refreshToken = ModelUtils.getRefreshTokenFrom(getTokenResponse());
            return refreshToken != null && !refreshToken.isExpired();
        } catch (Exception e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    @Override
    public void refresh() throws D4ScienceIAMClientException {
        try {
            KeycloakClient keycloakClient = iamClient.getKeycloakClient();
            this.tokenResponse = keycloakClient.refreshToken(
                    keycloakClient.getTokenEndpointURL(getIamClient().getRealmBaseURL()),
                    getTokenResponse());

        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

//    @Override
    public void refresh(String clientId, String clientSecret) throws D4ScienceIAMClientException {
        try {
            KeycloakClient keycloakClient = iamClient.getKeycloakClient();
            this.tokenResponse = keycloakClient.refreshToken(
                    keycloakClient.getTokenEndpointURL(getIamClient().getRealmBaseURL()), clientId, clientSecret,
                    getTokenResponse());

        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    @Override
    public Set<String> getGlobalRoles() throws D4ScienceIAMClientException {
        AccessToken accessToken = getAccessToken();
        return accessToken.getRealmAccess() != null ? accessToken.getRealmAccess().getRoles() : new HashSet<>();
    }

    @Override
    public Set<String> getRoles() throws D4ScienceIAMClientException {
        AccessToken accessToken = getAccessToken();
        Set<String> roles = getGlobalRoles();
        accessToken.getResourceAccess().forEach((r, a) -> roles.addAll(a.getRoles()));
        return roles;
    }

    @Override
    public Set<String> getResourceRoles(String resource) throws D4ScienceIAMClientException {
        AccessToken accessToken = getAccessToken();
        return accessToken.getResourceAccess() != null ? (accessToken.getResourceAccess().get(resource) != null
                ? accessToken.getResourceAccess().get(resource).getRoles()
                : new HashSet<>()) : new HashSet<>();
    }

    @Override
    public Set<String> getContextRoles() throws D4ScienceIAMClientException {
        AccessToken accessToken = getAccessToken();
        return accessToken.getResourceAccess() != null
                ? (accessToken.getResourceAccess().get(accessToken.getAudience()[0]) != null
                        ? accessToken.getResourceAccess().get(accessToken.getAudience()[0]).getRoles()
                        : new HashSet<>())
                : new HashSet<>();
    }

    @Override
    public String getName() throws D4ScienceIAMClientException {
        return getAccessToken().getName();
    }

    @Override
    public String getContactPerson() throws D4ScienceIAMClientException {
        AccessToken accessToken = getAccessToken();
        return (String) (accessToken.getOtherClaims() != null
                ? accessToken.getOtherClaims()
                        .get(D4ScienceCustomClaims.CLIENT_CONTACT_PERSON)
                : null);
    }

    @Override
    public String getContactOrganization() throws D4ScienceIAMClientException {
        AccessToken accessToken = getAccessToken();
        return (String) (accessToken.getOtherClaims() != null
                ? accessToken.getOtherClaims()
                        .get(D4ScienceCustomClaims.CLIENT_CONTACT_ORGANISATION)
                : null);
    }

    @Override
    public boolean isAccessTokenValid() throws D4ScienceIAMClientException {
        return isAccessTokenValid(true);
    }

    @Override
    public boolean isAccessTokenValid(boolean checkExpiration) throws D4ScienceIAMClientException {
        KeycloakClient keycloakClient = iamClient.getKeycloakClient();
        try {
            return ModelUtils.isValid(getAccessTokenString(),
                    keycloakClient.getRealmInfo(iamClient.getRealmBaseURL()).getPublicKey(),
                    checkExpiration);
        } catch (Exception e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    @Override
    public void verifyAccessToken() throws SignatureException, ExpiredJwtException, D4ScienceIAMClientException {
        try {
            getIamClient().verifyToken(getAccessTokenString());
        } catch (ExpiredJwtException | SignatureException e) {
            throw e;
        } catch (Exception e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    @Override
    public boolean isRefreshTokenValid() throws D4ScienceIAMClientException {
        return isRefreshTokenValid(true);
    }

    @Override
    public boolean isRefreshTokenValid(boolean checkExpiration) throws D4ScienceIAMClientException {
        KeycloakClient keycloakClient = iamClient.getKeycloakClient();
        String refreshTokenString = getRefreshTokenString();
        try {
            return refreshTokenString != null && ModelUtils.isValid(refreshTokenString,
                    keycloakClient.getRealmInfo(iamClient.getRealmBaseURL()).getPublicKey(),
                    checkExpiration);
        } catch (Exception e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    @Override
    public void verifyRefreshToken() throws SignatureException, ExpiredJwtException, D4ScienceIAMClientException {
        String refreshTokenString = getRefreshTokenString();
        if (refreshTokenString == null) {
            try {
                getIamClient().verifyToken(refreshTokenString);
            } catch (ExpiredJwtException | SignatureException e) {
                throw e;
            } catch (Exception e) {
                throw new D4ScienceIAMClientException(e);
            }
        }
    }
}