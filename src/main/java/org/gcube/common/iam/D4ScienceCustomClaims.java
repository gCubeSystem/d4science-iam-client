package org.gcube.common.iam;

public class D4ScienceCustomClaims {

    /**
     * The display name of a client with service account
     */
    public static final String CLIENT_NAME = "name";

    /**
     * The registered contact person of a client with service account
     */
    public static final String CLIENT_CONTACT_PERSON = "contact_person";

    /**
     * The registered contact organization of a client with service account
     */
    public static final String CLIENT_CONTACT_ORGANISATION = "contact_organisation";

}
