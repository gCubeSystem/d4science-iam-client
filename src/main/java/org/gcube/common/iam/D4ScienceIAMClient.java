package org.gcube.common.iam;

import java.net.URL;
import java.util.List;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientException;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.PublishedRealmRepresentation;
import org.gcube.io.jsonwebtoken.ExpiredJwtException;
import org.gcube.io.jsonwebtoken.JwtException;
import org.gcube.io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class that acts as IAM client providing authentication and authorization using the IAM hiding the underlying implementation
 * 
 * @author Mauro Mugnaini
 */
public class D4ScienceIAMClient {

    protected static Logger logger = LoggerFactory.getLogger(D4ScienceIAMClient.class);

    public static boolean USE_DYNAMIC_SCOPES = true;

    private KeycloakClient keycloakClient;
    private URL realmBaseURL;

    /**
     * The public GW used to obtain the token, is not declared final since it can be changed if necessary at runtime
     */
    private static String GATEWAY_CLIENT_ID = "d4science-internal-gateway";

    /**
     * Sets the new default GW <code>clientId</code> used for all the queries to the IAM server.
     * Note: The operation will logged as WARN to be visible.
     * @param gatewayClientId the new GW <code>clientId</code>
     */
    public static void setDefaultGatewayClientID(String gatewayClientId) {
        logger.warn("The default GW clientId will be changed to: {}", gatewayClientId);
        D4ScienceIAMClient.GATEWAY_CLIENT_ID = gatewayClientId;
    }

    /**
     * Creates a new client for the specific context, in the default IAM realm.
     * @param contextInfra the context to be used to obtain the base URL of the infrastructure
     * @return the client to be used for authn and authz requests
     * @throws D4ScienceIAMClientException if an error occurs obtaining the base URL
     */
    public static D4ScienceIAMClient newInstance(String contextInfra) throws D4ScienceIAMClientException {
        KeycloakClient keycloakClient = KeycloakClientFactory.newInstance()
                .useDynamicScopeInsteadOfCustomHeaderForContextRestricion(USE_DYNAMIC_SCOPES);

        logger.debug("Creating new D4ScienceIAMClient with infastructure context: {} ", contextInfra);
        try {
            return new D4ScienceIAMClient(keycloakClient, keycloakClient.getRealmBaseURL(contextInfra));
        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    /**
     * Creates a new client for the specific context, in the default realm.
     * @param contextInfra the context to be used to obtain the base URL of the infrastructure
     * @param realm the IAM realm
     * @return the client to be used for authn and authz requests
     * @throws D4ScienceIAMClientException if an error occurs obtaining the base URL
     */
    public static D4ScienceIAMClient newInstance(String contextInfra, String realm) throws D4ScienceIAMClientException {
        KeycloakClient keycloakClient = KeycloakClientFactory.newInstance()
                .useDynamicScopeInsteadOfCustomHeaderForContextRestricion(USE_DYNAMIC_SCOPES);

        logger.debug("Creating new D4ScienceIAMClient with infastructure context: {}, and realm: {}", contextInfra, realm);
        try {
            return new D4ScienceIAMClient(keycloakClient, keycloakClient.getRealmBaseURL(contextInfra, realm));
        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    /**
     * Creates a new client with the provided base URL.
     * @param realmBaseURL the realm base URL
     * @return the client to be used for authn and authz requests
     */
    public static D4ScienceIAMClient newInstance(URL realmBaseURL) {
        logger.debug("Creating new D4ScienceIAMClient with baase URL: {} ", realmBaseURL);
        return new D4ScienceIAMClient(KeycloakClientFactory.newInstance()
                .useDynamicScopeInsteadOfCustomHeaderForContextRestricion(USE_DYNAMIC_SCOPES), realmBaseURL);
    }

    private D4ScienceIAMClient(KeycloakClient keycloakClient, URL realmBaseURL) {
        this.keycloakClient = keycloakClient;
        this.realmBaseURL = realmBaseURL;
    }

    protected KeycloakClient getKeycloakClient() {
        return this.keycloakClient;
    }

    public URL getRealmBaseURL() {
        return this.realmBaseURL;
    }

    /**
     * Authenticates the client with provided id and secret
     * @param clientId the client id
     * @param clientSecret the client secret
     * @return the authn object
     * @throws D4ScienceIAMClientException if an error occurs during authn process
     */
    public D4ScienceIAMClientAuthn authenticate(String clientId, String clientSecret)
            throws D4ScienceIAMClientException {

        return authenticate(clientId, clientSecret, null);
    }

    /**
     * Authenticates the client with provided credentials, reducing the token audience to the requested `context`
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @return the authn object
     * @throws D4ScienceIAMClientException if an error occurs during authn process
     */
    public D4ScienceIAMClientAuthn authenticate(String clientId, String clientSecret, String context)
            throws D4ScienceIAMClientException {

        return new D4ScienceIAMClientAuthn4Client(this, clientId, clientSecret, context);
    }

    /**
     * Authenticates the user with provided username and password by using the default <code>clientId</code>.
     * @param username the user's username
     * @param password the user's password
     * @return the authn object
     * @throws D4ScienceIAMClientException if an error occurs during authn process
     * @see set
     * @deprecated this authn method is deprecated in the oauth2 specifications (see https://oauth.net/2/grant-types/password/)
     */
    public D4ScienceIAMClientAuthn authenticateUser(String username, String password)
            throws D4ScienceIAMClientException {

        return authenticateUser(GATEWAY_CLIENT_ID, null, username, password);
    }

    /**
     * Authenticates the user with provided username and password by using the default <code>clientId</code>.
     * @param username the user's username
     * @param password the user's password
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @return the authn object
     * @throws D4ScienceIAMClientException if an error occurs during authn process
     * @see set
     * @deprecated this authn method is deprecated in the oauth2 specifications (see https://oauth.net/2/grant-types/password/)
     */
    public D4ScienceIAMClientAuthn authenticateUser(String username, String password, String context)
            throws D4ScienceIAMClientException {

        return authenticateUser(GATEWAY_CLIENT_ID, null, username, password, context);
    }

    /**
     * Authenticates the user with provided username and password
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @return the authn object
     * @throws D4ScienceIAMClientException if an error occurs during authn process
     * @deprecated this authn method is deprecated in the oauth2 specifications (see https://oauth.net/2/grant-types/password/)
     */
    public D4ScienceIAMClientAuthn authenticateUser(String clientId, String clientSecret, String username, String password)
            throws D4ScienceIAMClientException {

        return authenticateUser(clientId, clientSecret, username, password, null);
    }

    /**
     * Authenticates the user with provided credentials, reducing the token audience to the requested `context`.
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param username the user's username
     * @param password the user's password
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @return the authn object
     * @throws D4ScienceIAMClientException if an error occurs during authn process
     * @deprecated this authn method is deprecated in the oauth2 specifications (see https://oauth.net/2/grant-types/password/)
     */
    public D4ScienceIAMClientAuthn authenticateUser(String clientId, String clientSecret, String username, String password, String context)
            throws D4ScienceIAMClientException {

        return new D4ScienceIAMClientAuthn4User(this, clientId, clientSecret, username, password, context);
    }

    /**
     * Directly authorizes the client by using the provided credentials, for the specific context audience and with no optional permissions
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @return the authz object
     * @throws D4ScienceIAMClientException if an error occurs during authz process
     */
    public D4ScienceIAMClientAuthz authorize(String clientId, String clientSecret, String context)
            throws D4ScienceIAMClientException {

        return authorize(clientId, clientSecret, context, null);
    }

    /**
     * Directly authorizes the client by using the provided credentials, for the specific context audience and with optional permissions
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @param permissions the optional permissions
     * @return the authz object
     * @throws D4ScienceIAMClientException if an error occurs during authz process
     */
    public D4ScienceIAMClientAuthz authorize(String clientId, String clientSecret, String context,
            List<String> permissions) throws D4ScienceIAMClientException {

        return new D4ScienceIAMClientAuthz(this, clientId, clientSecret, context, permissions);
    }

    /**
     * Verifies the token signature and expiration
     * 
     * @param token the base64 JWT token string
     * @throws SignatureException if the token signature is invalid
     * @throws ExpiredJwtException if the token is expired
     * @throws JwtException if another JWT related problem is found
     * @throws Exception if an unexpected error occurs (e.g. constructing the verifier)
     */
    public void verifyToken(String token) throws SignatureException, ExpiredJwtException, JwtException, Exception {
        PublishedRealmRepresentation realmInfo = keycloakClient.getRealmInfo(realmBaseURL);
        ModelUtils.verify(token, realmInfo.getPublicKey());
    }

}