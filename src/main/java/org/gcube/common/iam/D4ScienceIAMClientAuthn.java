package org.gcube.common.iam;

import java.util.List;

import org.gcube.common.keycloak.model.TokenResponse;

public abstract class D4ScienceIAMClientAuthn extends AbstractIAMResponse {

    public D4ScienceIAMClientAuthn(D4ScienceIAMClient iamClient, TokenResponse tokenResponse) {
        super(iamClient, tokenResponse);
    }

    /**
     * Authorizes the client or the user by using the authn already obtained, for the specific context audience and no optional permissions.
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @return the authz object
     * @throws D4ScienceIAMClientException if an error occurs during authz process
     */
    public D4ScienceIAMClientAuthz authorize(String context) throws D4ScienceIAMClientException {
        return authorize(context, null);
    }

    /**
     * Authorizes the client or the user by using the authn already obtained, for the specific context audience and with optional permissions.
     * @param authn 
     * @param context the requested token context audience (e.g. a specific context or another client)
     * @param permissions the optional permissions
     * @return the authz object
     * @throws D4ScienceIAMClientException if an error occurs during authz process
     */
    public D4ScienceIAMClientAuthz authorize(String context, List<String> permissions)
            throws D4ScienceIAMClientException {

        return new D4ScienceIAMClientAuthz(this, context, permissions);
    }

}