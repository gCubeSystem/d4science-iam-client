package org.gcube.common.iam;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientException;
import org.gcube.common.keycloak.model.TokenResponse;

public class D4ScienceIAMClientAuthn4Client extends D4ScienceIAMClientAuthn {

    protected D4ScienceIAMClientAuthn4Client(D4ScienceIAMClient iamClient, String clientId, String clientSecret)
            throws D4ScienceIAMClientException {

        this(iamClient, clientId, clientSecret, null);
    }

    protected D4ScienceIAMClientAuthn4Client(D4ScienceIAMClient iamClient, String clientId, String clientSecret,
            String context) throws D4ScienceIAMClientException {

        super(iamClient, performClientAuthn(iamClient, clientId, clientSecret, context));
    }

    protected static final TokenResponse performClientAuthn(D4ScienceIAMClient iamClient, String clientId,
            String clientSecret, String context) throws D4ScienceIAMClientException {

        KeycloakClient keycloakClient = iamClient.getKeycloakClient();
        try {
            return keycloakClient.queryOIDCTokenWithContext(
                    keycloakClient.getTokenEndpointURL(iamClient.getRealmBaseURL()), clientId, clientSecret, context);

        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

}