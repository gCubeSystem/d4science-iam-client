package org.gcube.common.iam;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientException;
import org.gcube.common.keycloak.model.TokenResponse;

public class D4ScienceIAMClientAuthn4User extends D4ScienceIAMClientAuthn {

    protected D4ScienceIAMClientAuthn4User(D4ScienceIAMClient iamClient, String clientId, String clientSecret,
            String username, String password) throws D4ScienceIAMClientException {

        this(iamClient, clientId, clientSecret, username, password, null);
    }

    protected D4ScienceIAMClientAuthn4User(D4ScienceIAMClient iamClient, String clientId, String clientSecret,
            String username, String password, String context) throws D4ScienceIAMClientException {

        super(iamClient, performUserAuthn(iamClient, clientId, clientSecret, username, password, context));
    }

    protected static final TokenResponse performUserAuthn(D4ScienceIAMClient iamClient, String clientId,
            String clientSecret, String username, String password, String context) throws D4ScienceIAMClientException {

        KeycloakClient keycloakClient = iamClient.getKeycloakClient();
        try {
            return keycloakClient.queryOIDCTokenOfUserWithContext(
                    keycloakClient.getTokenEndpointURL(iamClient.getRealmBaseURL()), clientId, clientSecret, username,
                    password, context);

        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

}