package org.gcube.common.iam;

import java.util.List;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientException;
import org.gcube.common.keycloak.model.TokenResponse;

public class D4ScienceIAMClientAuthz extends AbstractIAMResponse {

    protected D4ScienceIAMClientAuthz(D4ScienceIAMClientAuthn authn, String context, List<String> permissions)
            throws D4ScienceIAMClientException {

        super(authn.getIamClient(), performAuthz(authn.getIamClient(), authn.getTokenResponse(), context, permissions));
    }

    private static final TokenResponse performAuthz(D4ScienceIAMClient iamClient, TokenResponse authnTR,
            String context, List<String> permissions) throws D4ScienceIAMClientException {

        KeycloakClient keycloakClient = iamClient.getKeycloakClient();
        try {
            return keycloakClient.queryUMAToken(keycloakClient.getTokenEndpointURL(iamClient.getRealmBaseURL()),
                    authnTR, context, permissions);

        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

    protected D4ScienceIAMClientAuthz(D4ScienceIAMClient iamClient, String clientId, String clientSecret,
            String context, List<String> permissions) throws D4ScienceIAMClientException {

        super(iamClient, performAuthz(iamClient, clientId, clientSecret, context, permissions));
    }

    private static final TokenResponse performAuthz(D4ScienceIAMClient iamClient, String clientId, String clientSecret,
            String context, List<String> permissions) throws D4ScienceIAMClientException {

        KeycloakClient keycloakClient = iamClient.getKeycloakClient();
        try {
            return keycloakClient.queryUMAToken(keycloakClient.getTokenEndpointURL(iamClient.getRealmBaseURL()),
                    clientId, clientSecret, context, permissions);

        } catch (KeycloakClientException e) {
            throw new D4ScienceIAMClientException(e);
        }
    }

}