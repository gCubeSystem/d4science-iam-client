package org.gcube.common.iam;

import org.gcube.common.keycloak.KeycloakClientException;

public class D4ScienceIAMClientException extends Exception {

    private static final long serialVersionUID = 468793481934475559L;

    public D4ScienceIAMClientException(Throwable cause) {
        super(cause);
    }

    public D4ScienceIAMClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getStatus() {
        return getCause() instanceof KeycloakClientException ? ((KeycloakClientException) getCause()).getStatus() : -1;
    }

    public String getContentType() {
        return getCause() instanceof KeycloakClientException ? ((KeycloakClientException) getCause()).getContentType()
                : null;
    }

    public boolean hasJSONPayload() {
        return getCause() instanceof KeycloakClientException ? ((KeycloakClientException) getCause()).hasJSONPayload()
                : false;
    }

    public String getResponseString() {
        return getCause() instanceof KeycloakClientException
                ? ((KeycloakClientException) getCause()).getResponseString()
                : null;
    }

}
