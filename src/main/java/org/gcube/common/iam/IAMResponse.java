package org.gcube.common.iam;

import java.util.Set;

import org.gcube.common.keycloak.model.AccessToken;
import org.gcube.io.jsonwebtoken.ExpiredJwtException;
import org.gcube.io.jsonwebtoken.security.SignatureException;

public interface IAMResponse {

    /**
     * Returns the access token in the response.
     * @return The access token
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    public AccessToken getAccessToken() throws D4ScienceIAMClientException;

    /**
     * Returns the access token in the response as string.
     * @return The access token as string
     */
    String getAccessTokenString();

    /**
     * Check if the current response is expired
     * @return <code>true</code> if the response is expired, <code>false</code> otherwise
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    boolean isExpired() throws D4ScienceIAMClientException;

    /**
     * Check if the current response can be refreshed
     * @return <code>true</code> if the response can be refreshed, <code>false</code> otherwise
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    boolean canBeRefreshed() throws D4ScienceIAMClientException;

    /**
     * Refreshes the current response, new data can be obtained again with accessors.
     * @throws D4ScienceIAMClientException if something goes wrong during the token refresh
     */
    void refresh() throws D4ScienceIAMClientException;

    /**
     * Returns the resource roles for the resource specified in the token context
     * @return the token context's roles
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    Set<String> getContextRoles() throws D4ScienceIAMClientException;

    /**
     * Returns the resource roles for the resource specified in the resource parameter
     * @param resource the resource of which obtain the roles
     * @return the roles for the resource
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    Set<String> getResourceRoles(String resource) throws D4ScienceIAMClientException;

    /**
     * Returns all the roles, realm and from all the resources in the token in the same set
     * @return the union of all the roles in the token
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    Set<String> getRoles() throws D4ScienceIAMClientException;

    /**
     * Returns the realm roles in the token
     * @return the realm roles
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    Set<String> getGlobalRoles() throws D4ScienceIAMClientException;

    /**
     * Returns the client's contact organization from the token
     * @return the contact organization string
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    String getContactOrganization() throws D4ScienceIAMClientException;

    /**
     * Returns the client's contact person from the token
     * @return the contact person string
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    String getContactPerson() throws D4ScienceIAMClientException;

    /**
     * Returns the client's name from the token
     * @return the name string
     * @throws D4ScienceIAMClientException if something goes wrong during the token decoding or JSON parsing
     */
    String getName() throws D4ScienceIAMClientException;

    /**
     * Quick way to check if the access token is valid by checking the digital signature and the token expiration
     * @return <code>true</code> if the access token is valid, <code>false</code> otherwise
     * @throws D4ScienceIAMClientException if something goes wrong during the token validity checks
     */
    boolean isAccessTokenValid() throws D4ScienceIAMClientException;

    /**
     * Quick way to check if the access token is valid by checking the digital signature and the token expiration if the <code>checkExpiration</code> parameter is <code>true</code>
     * @param checkExpiration checks also if the token is expired
     * @return <code>true</code> if the access token is valid, <code>false</code> otherwise
     * @throws D4ScienceIAMClientException if something goes wrong during the token validity checks
     */
    boolean isAccessTokenValid(boolean checkExpiration) throws D4ScienceIAMClientException;

    /**
     * Verifies the access token integrity and validity; token digital signature and expiration are reported via specific exceptions.
     * @throws SignatureException if the token has been tampered and/or signature is invalid
     * @throws ExpiredJwtException if the token validity is expired
     * @throws D4ScienceIAMClientException if something else goes wrong during the token verification
     */
    void verifyAccessToken() throws SignatureException, ExpiredJwtException, D4ScienceIAMClientException;

    /**
     * Quick way to check if the refresh token present in the current response and it is valid by checking the digital signature and the token expiration
     * @return <code>true</code> if the refresh token is valid, <code>false</code> otherwise
     * @throws D4ScienceIAMClientException if something goes wrong during the token validity checks
     */
    boolean isRefreshTokenValid() throws D4ScienceIAMClientException;

    /**
     * Quick way to check if the refresh token present in the current response and it is valid by checking the digital signature and the token
     * expiration if the <code>checkExpiration</code> parameter is <code>true</code>
     * @param checkExpiration checks also if the token is expired
     * @return <code>true</code> if the refresh token is valid, <code>false</code> otherwise
     * @throws D4ScienceIAMClientException if something goes wrong during the token validity checks
     */
    boolean isRefreshTokenValid(boolean checkExpiration) throws D4ScienceIAMClientException;

    /**
     * Verifies the refresh token integrity and validity; token digital signature and expiration are reported via specific exceptions.
     * @throws SignatureException if the token has been tampered and/or signature is invalid
     * @throws ExpiredJwtException if the token validity is expired
     * @throws D4ScienceIAMClientException if something else goes wrong during the token verification
     */
    void verifyRefreshToken() throws SignatureException, ExpiredJwtException, D4ScienceIAMClientException;

}