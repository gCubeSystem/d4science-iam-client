package org.gcube.common.iam;

import java.net.URL;

import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.TokenResponse;

public class OIDCBearerAuth extends AbstractIAMResponse {

    protected OIDCBearerAuth(D4ScienceIAMClient iamClient, TokenResponse tokenResponse) {
        super(iamClient, tokenResponse);
    }

    /**
     * Constructs a new object from an HTTP authorization header containing the bearer token with an OIDC access-token.
     * @param authorizationHeader the HTTP authorization header
     * @return the auth object
     */
    public static OIDCBearerAuth fromAuthorizationHeader(String authorizationHeader) {
        return fromBearerAuthorization(authorizationHeader);
    }

    /**
     * Constructs a new object from a bearer token with an OIDC access-token.
     * @param bearerAuthorization the bearer token (with or without 'bearer ' prefix
     * @return the auth object
     */
    public static OIDCBearerAuth fromBearerAuthorization(String bearerAuthorization) {
        return fromAccessTokenString(bearerAuthorization.replace("bearer ", ""));
    }

    /**
     * Constructs a new object from an OIDC base64 encoded access-token string.
     * @param accessToken the OIDC base64 encoded access-token string
     * @return the auth object
     */
    public static OIDCBearerAuth fromAccessTokenString(String accessToken) {
        TokenResponse tr = new TokenResponse();
        tr.setAccessToken(accessToken);
        try {
            return new OIDCBearerAuth(
                    D4ScienceIAMClient.newInstance(new URL(ModelUtils.getAccessTokenFrom(tr).getIssuer())),
                    tr);
        } catch (Exception e) {
            // This case is almost impossible if the access token is parse correctly and contains the mandatory `iss` claim
            return null;
        }
    }

}
