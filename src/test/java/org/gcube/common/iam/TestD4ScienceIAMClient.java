package org.gcube.common.iam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Set;

import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.io.jsonwebtoken.ExpiredJwtException;
import org.gcube.io.jsonwebtoken.security.SignatureException;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestD4ScienceIAMClient {

    protected static final Logger logger = LoggerFactory.getLogger(TestD4ScienceIAMClient.class);

    protected static final String DEV_ROOT_CONTEXT = "/gcube";
    protected static final String CLIENT_ID = "keycloak-client-unit-test";
    protected static final String CLIENT_SECRET = "ebf6f82e-9511-408e-8321-203081e472d8";
    protected static final String TEST_AUDIENCE = "conductor-server";
    protected static final String GATEWAY = "next.dev.d4science.org";
    protected static final String TEST_USER_USERNAME = "testuser";
    protected static final String TEST_USER_PASSWORD = "t35tp455";

    protected static final Set<String> CLIENT_REALM_ROLES = Collections.singleton("d4s-client");
    protected static final Set<String> CONDUCTOR_SERVER_ROLES = Collections.singleton("dummy-test-role");
    protected static final Set<String> UMA_PROTECTION_ROLE = Collections.singleton("uma_protection");

    protected static final String NAME = "Keycloak-client unit-test";
    protected static final String CONTACT_PERSON = "mauro.mugnaini";
    protected static final String CONTACT_ORGANIZATION = "Nubisware S.r.l.";

    protected static final Set<String> USER_REALM_ROLES = Collections.singleton("IS-Manager");
    protected static final String TOKEN_RESTRICTION_VRE_CONTEXT = "%2Fgcube%2Fdevsec%2FCCP";
    protected static final String MEMBER_ROLE = "Member";

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test1Authn() throws Exception {
        logger.info("*** [1] Testing authentication");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authenticate(CLIENT_ID,
                CLIENT_SECRET);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertEquals("Realm roles are not as expected.", CLIENT_REALM_ROLES, authn.getGlobalRoles());
        assertEquals("Name is not the expected.", NAME, authn.getName());
        assertEquals("Contact person is not the expected.", CONTACT_PERSON, authn.getContactPerson());
        assertEquals("Contat organization is not the expected.", CONTACT_ORGANIZATION, authn.getContactOrganization());
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());

        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh(CLIENT_ID, CLIENT_SECRET);
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @Test
    public void test2AuthnWithSpecificAudience() throws Exception {
        logger.info("*** [2] Testing authentication");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authenticate(CLIENT_ID,
                CLIENT_SECRET, TEST_AUDIENCE);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertEquals(TEST_AUDIENCE, authn.getAccessToken().getAudience()[0]);
        assertEquals(CONDUCTOR_SERVER_ROLES, authn.getContextRoles());
        assertEquals(CLIENT_REALM_ROLES, authn.getGlobalRoles());
        assertEquals(NAME, authn.getName());
        assertEquals(CONTACT_PERSON, authn.getContactPerson());
        assertEquals(CONTACT_ORGANIZATION, authn.getContactOrganization());
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());

        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh(CLIENT_ID, CLIENT_SECRET);
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @Test
    public void test3AuthnAndAuthz() throws Exception {
        logger.info("*** [3] Testing authentication and then authorization");
        D4ScienceIAMClientAuthz authz = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT)
                .authenticate(CLIENT_ID, CLIENT_SECRET).authorize(TEST_AUDIENCE, null);

        logger.info("Authz scope: {}", authz.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authz.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authz.getAccessToken().getResourceAccess());
        logger.info("Authz ALL roles: {}", authz.getRoles());
        logger.info("Authz REALM roles: {}", authz.getGlobalRoles());
        logger.info("Authz AUDIENCE's roles: {}", authz.getContextRoles());
        logger.info("Authz D4S identity: '{}' by {} [{}]", authz.getName(), authz.getContactPerson(),
                authz.getContactOrganization());

        logger.info("Authz access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authz.getTokenResponse()));

        assertEquals(TEST_AUDIENCE, authz.getAccessToken().getAudience()[0]);
        assertEquals(CONDUCTOR_SERVER_ROLES, authz.getContextRoles());
        assertEquals(CONDUCTOR_SERVER_ROLES, authz.getResourceRoles(TEST_AUDIENCE));
        assertEquals(CLIENT_REALM_ROLES, authz.getGlobalRoles());
        assertEquals(NAME, authz.getName());
        assertEquals(CONTACT_PERSON, authz.getContactPerson());
        assertEquals(CONTACT_ORGANIZATION, authz.getContactOrganization());
        assertTrue("The authz access token is not valid.", authz.isAccessTokenValid());
    }

    @Test
    public void test4DirectAuthz() throws Exception {
        logger.info("*** [4] Testing direct authorization");
        D4ScienceIAMClientAuthz authz = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authorize(CLIENT_ID,
                CLIENT_SECRET, TEST_AUDIENCE, null);

        logger.info("Authz scope: {}", authz.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authz.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authz.getAccessToken().getResourceAccess());
        logger.info("Authz ALL roles: {}", authz.getRoles());
        logger.info("Authz REALM roles: {}", authz.getGlobalRoles());
        logger.info("Authz AUDIENCE's roles: {}", authz.getContextRoles());
        logger.info("Authz D4S identity: '{}' by {} [{}]", authz.getName(), authz.getContactPerson(),
                authz.getContactOrganization());

        logger.info("Authz access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authz.getTokenResponse()));

        assertEquals(TEST_AUDIENCE, authz.getAccessToken().getAudience()[0]);
        assertEquals(TEST_AUDIENCE, authz.getAccessToken().getAudience()[0]);
        assertEquals(CONDUCTOR_SERVER_ROLES, authz.getContextRoles());
        assertEquals(CONDUCTOR_SERVER_ROLES, authz.getResourceRoles(TEST_AUDIENCE));
        assertEquals(CLIENT_REALM_ROLES, authz.getGlobalRoles());
        assertEquals(NAME, authz.getName());
        assertEquals(CONTACT_PERSON, authz.getContactPerson());
        assertEquals(CONTACT_ORGANIZATION, authz.getContactOrganization());
        assertTrue("The authz access token is not valid.", authz.isAccessTokenValid());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test50UserAuthnOnDefaultGateway() throws Exception {
        logger.info("*** [5.0] Testing user authentication on the default gateway");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT)
                .authenticateUser(TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertTrue("Realm roles are not as expected.", authn.getGlobalRoles().containsAll(USER_REALM_ROLES));
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());
        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh();
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test51UserAuthnWithClient() throws Exception {
        logger.info("*** [5.1] Testing user authentication");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authenticateUser(CLIENT_ID,
                CLIENT_SECRET, TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertEquals("Realm roles are not as expected.", USER_REALM_ROLES, authn.getGlobalRoles());
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());
        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh(CLIENT_ID, CLIENT_SECRET);
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test52UserAuthnOnSpecificGW() throws Exception {
        logger.info("*** [5.2] Testing user authentication using gateway client");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authenticateUser(GATEWAY, null,
                TEST_USER_USERNAME, TEST_USER_PASSWORD);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertTrue("Realm roles are not as expected.", authn.getGlobalRoles().containsAll(USER_REALM_ROLES));
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());

        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh();
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test60UserAuthnOnDefaultGatewayWithSpecificAudience() throws Exception {
        logger.info("*** [6.0] Testing user authentication on the default gateway for a scpeific context");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT)
                .authenticateUser(TEST_USER_USERNAME, TEST_USER_PASSWORD, TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertTrue("Realm roles are not as expected.", authn.getGlobalRoles().containsAll(USER_REALM_ROLES));
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());

        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh();
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test61UserAuthnWithSpecificAudience() throws Exception {
        logger.info("*** [6.1] Testing user authentication for a scpeific context");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authenticateUser(CLIENT_ID,
                CLIENT_SECRET, TEST_USER_USERNAME, TEST_USER_PASSWORD, TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, authn.getAccessToken().getAudience()[0]);
        assertEquals(USER_REALM_ROLES, authn.getGlobalRoles());
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());

        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh(CLIENT_ID, CLIENT_SECRET);
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test62UserAuthnWithSpecificAudienceOnGW() throws Exception {
        logger.info("*** [6.1] Testing user authentication for a scpeific context using gateway client");
        D4ScienceIAMClientAuthn authn = D4ScienceIAMClient.newInstance(DEV_ROOT_CONTEXT).authenticateUser(GATEWAY,
                null, TEST_USER_USERNAME, TEST_USER_PASSWORD, TOKEN_RESTRICTION_VRE_CONTEXT);

        logger.info("Authn scope: {}", authn.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) authn.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", authn.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", authn.getRoles());
        logger.info("Authn REALM roles: {}", authn.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", authn.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", authn.getName(), authn.getContactPerson(),
                authn.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(authn.getTokenResponse()));

        assertEquals(TOKEN_RESTRICTION_VRE_CONTEXT, authn.getAccessToken().getAudience()[0]);
        assertTrue("Realm roles are not as expected.", authn.getGlobalRoles().containsAll(USER_REALM_ROLES));
        assertTrue("The authn access token is not valid.", authn.isAccessTokenValid());

        if (authn.canBeRefreshed()) {
            logger.info("Refreshing token...");
            authn.refresh();
        } else {
            logger.warn("Token cannot be refreshed");
        }
    }

    @Test(expected = ExpiredJwtException.class)
    public void test7OIDCBearerAuth() throws Exception {
        String bearer = "";
        try (InputStreamReader isr = new InputStreamReader(
                getClass().getClassLoader().getResourceAsStream("access-token-b64-encoded.txt"))) {

            bearer = "bearer " + new BufferedReader(isr).readLine();
        }
        OIDCBearerAuth auth = OIDCBearerAuth.fromBearerAuthorization(bearer);
        logger.info("Authn scope: {}", auth.getAccessToken().getScope());
        logger.info("Authn AUDIENCE: {}", (Object[]) auth.getAccessToken().getAudience());
        logger.info("Authn RESOURCE ACCESS: {}", auth.getAccessToken().getResourceAccess());
        logger.info("Authn ALL roles: {}", auth.getRoles());
        logger.info("Authn REALM roles: {}", auth.getGlobalRoles());
        logger.info("Authn AUDIENCE's roles: {}", auth.getContextRoles());
        logger.info("Authn D4S identity: '{}' by {} [{}]", auth.getName(), auth.getContactPerson(),
                auth.getContactOrganization());

        logger.info("Authn access token:\n{}",
                ModelUtils.getAccessTokenPayloadJSONStringFrom(auth.getTokenResponse()));

        assertEquals("Realm roles are not as expected.", CLIENT_REALM_ROLES, auth.getGlobalRoles());
        assertEquals("Name is not the expected.", NAME, auth.getName());
        assertEquals("Contact person is not the expected.", CONTACT_PERSON, auth.getContactPerson());
        assertEquals("Contat organization is not the expected.", CONTACT_ORGANIZATION, auth.getContactOrganization());
        assertFalse("The authn access token is not valid.", auth.isAccessTokenValid());
        try {
            auth.verifyAccessToken();
        } catch (ExpiredJwtException e) {
            // This is OK in the test
            logger.info("Token is correctly expired");
            throw e;
        } catch (SignatureException e) {
            // This is not OK in the test
            logger.error("Token signature is invalid", e);
            throw e;
        } catch (D4ScienceIAMClientException e) {
            logger.error("Something else has gone wrong during token verification", e);
            throw e;
        }
    }

}
